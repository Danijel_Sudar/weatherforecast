package com.danijelsudar.danijel.weatherforecast

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_cities.*
import kotlinx.android.synthetic.main.list_cities_item.view.*
import java.util.*
import com.bumptech.glide.Glide
import com.danijelsudar.danijel.weatherforecast.model.CityWeatherData
import com.danijelsudar.danijel.weatherforecast.net.APIService
import com.danijelsudar.danijel.weatherforecast.net.ApiUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import android.net.ConnectivityManager
import android.provider.Settings
import com.danijelsudar.danijel.weatherforecast.model.ListData
import android.support.v7.app.AlertDialog


class CitiesFragment : Fragment() {
    private var mAdapter: WFAdapter? = null
    private var listCities = ArrayList<City>()
    /*
    * header iz Drawer menia gde treba staviti grad i temperaturu
    * */
    private lateinit var header: DrawerItem
    private val TAG = "CitiesFragment"

    private var mAPIService: APIService? = null
    private val APP_KEY = "17e60d04fcefa9570f820757322be07a"
    private var COUNT_CITIES = 30
    /*
    * pozicija selektovanog grada
    * */
    private var mSelected: Int = -1

    companion object {
        private val KEY_NAV_MENU = "menu"

        fun newInstance(header: DrawerItem): CitiesFragment {
            val fragment = CitiesFragment()
            val args = Bundle()
            args.putSerializable(KEY_NAV_MENU, header)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        header = arguments.getSerializable(KEY_NAV_MENU) as DrawerItem
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_cities, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler_view_cities.layoutManager = LinearLayoutManager(activity)
        updateUI()
    }

    override fun onStart() {
        super.onStart()
        if(listCities.size == 0)loadData()
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun  ArrayList<City>.setNewItems(list: List<ListData>, selectedCity: String){
        list?.sortedBy { it.name }.forEach{
            var city = City("${it.id}","${it.name}","${it.getData()}",selectedCity == it.name,"${it.weather[0].getImageURL()}","${it.main.temp}")
            this.add(city)
        }
    }

    protected fun createNetErrorDialog() {

        val builder = AlertDialog.Builder(context)
        builder.setMessage("You need a network connection to use this application. Please turn on mobile network or Wi-Fi in Settings.")
                .setTitle("Unable to connect")
                .setCancelable(false)
                .setPositiveButton(
                        "Settings",
                        { _, _ ->
                            startActivity(Intent(Settings.ACTION_WIRELESS_SETTINGS))
                        }
                )
                .setNegativeButton(
                        "Cancel",
                         { _, _ ->
                             activity.finish()
                         }
                )
        val alert = builder.create()
        alert.show()
    }

    /*
    * ucitava podatke sa interneta
    * */
    private fun loadData() {
        progressBar.visibility = View.VISIBLE

        if(!isNetworkAvailable()) {
            createNetErrorDialog()
        }

        mAPIService = ApiUtils.getAPIService()
        mAPIService!!.citiesInCycle(
                lat = "45.11",
                lon = "19.84",
                cnt = COUNT_CITIES,
                APPID = APP_KEY/*,
                cb = callback*/
        ).enqueue(object: Callback<CityWeatherData> {
            override fun onFailure(call: Call<CityWeatherData>?, t: Throwable?) {
                Log.d(TAG, "error loading from API" + t)
            }

            override fun onResponse(call: Call<CityWeatherData>?, response: Response<CityWeatherData>?) {
                Log.d(TAG, "posts loaded from API: ${(response?.isSuccessful)}")

                var item = response?.body() as CityWeatherData
                var selectedCity = header?.mTitle ?: ""
                listCities.setNewItems(item?.list, selectedCity)
                updateUI()
                progressBar.visibility = View.GONE
            }
        })
    }

    fun changeHeader(city: String? = null, temperature: String = ""){
        header.mTitle = city
        header.mTemperature = "$temperature°"
    }

    private fun updateUI() {
        if(mAdapter == null || recycler_view_cities.adapter == null) {
            mAdapter = WFAdapter(listCities)
            recycler_view_cities.adapter = mAdapter
        } else {
            mAdapter?.notifyDataSetChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        updateUI()
    }

    private inner class WFAdapter(cities: ArrayList<City>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private var mCities = cities

        override fun getItemCount(): Int {
            return mCities.size
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
            return WFViewHolder(parent, LayoutInflater.from(parent!!.context))
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
            (holder as WFViewHolder).bind(mCities[position])
            SelectRadio(holder,position)
        }

        private fun SelectRadio(holder: RecyclerView.ViewHolder?, position: Int){
            (holder as WFViewHolder).itemView?.city_radio_button?.setOnClickListener {
                when (mSelected) {
                    position -> {
                        mCities[position].mIsChecked = !mCities[position].mIsChecked
                        notifyItemChanged(position)
                        mSelected = -1
                        changeHeader()
                    }
                    -1 -> {
                        mCities[position].mIsChecked = !mCities[position].mIsChecked
                        notifyItemChanged(position)
                        mSelected = position
                        changeHeader(city = mCities[position].mCity, temperature = mCities[position].mWeather)
                    }
                    else -> {
                        mCities[position].mIsChecked = !mCities[position].mIsChecked
                        mCities[mSelected].mIsChecked = !mCities[mSelected].mIsChecked
                        notifyItemChanged(position)
                        notifyItemChanged(mSelected)
                        mSelected = position
                        changeHeader(city = mCities[position].mCity, temperature = mCities[position].mWeather)
                    }
                }
            }
        }
    }

    private inner class WFViewHolder(parent: ViewGroup?, inflater: LayoutInflater?):
            RecyclerView.ViewHolder(inflater?.inflate(R.layout.list_cities_item,parent,false)){
        private lateinit var mCity: City

        private fun setSelected(){
            itemView?.city_text_view?.setTextColor(resources.getColor(R.color.colorPrimary))
            itemView?.time_text_view?.setTextColor(resources.getColor(R.color.colorPrimary))
            itemView.setBackgroundColor(resources.getColor(R.color.selected))
        }
        private fun notSelected(){
            itemView?.setBackgroundColor(resources.getColor(R.color.white))
            itemView?.city_text_view?.setTextColor(resources.getColor(R.color.text))
            itemView?.time_text_view?.setTextColor(resources.getColor(R.color.text))

        }

        fun bind(city: City){
            mCity = city
            itemView?.city_text_view?.text = mCity.mCity ?: ""
            itemView?.city_radio_button?.isChecked = mCity?.mIsChecked as Boolean
            itemView?.time_text_view?.text = mCity?.mDate ?: ""
            if(mCity?.mIsChecked as Boolean) setSelected() else notSelected()
            Glide.with(activity).load(mCity.mImage).into(itemView?.image_weather)
            Glide.with(activity).load(mCity.mImage).into(itemView?.image_weather)
        }
    }

}
