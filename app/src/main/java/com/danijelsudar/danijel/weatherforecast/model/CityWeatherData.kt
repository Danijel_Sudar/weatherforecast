package com.danijelsudar.danijel.weatherforecast.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

data class CityWeatherData (
    @SerializedName("message")
    @Expose
    var message:String,
    
    @SerializedName("cod")
    @Expose
    var cod:String,
    
    @SerializedName("count")
    @Expose
    var count:Int,
    
    @SerializedName("list")
    @Expose
    var list:List<ListData>
)

data class Clouds (
    @SerializedName("all")
    @Expose
    var all:Int
)

data class Coord (
    @SerializedName("lat")
    @Expose
    var lat:Double,

    @SerializedName("lon")
    @Expose
    var lon:Double
)

data class ListData (
    @SerializedName("id")
    @Expose
    var id:Int,

    @SerializedName("name")
    @Expose
    var name:String,

    @SerializedName("coord")
    @Expose
    var coord:Coord,

    @SerializedName("main")
    @Expose
    var main:Main,

    @SerializedName("dt")
    @Expose
    var dt:Int,

    @SerializedName("wind")
    @Expose
    var wind:Wind,

    @SerializedName("sys")
    @Expose
    var sys:Sys,

    @SerializedName("rain")
    @Expose
    var rain:Any,

    @SerializedName("snow")
    @Expose
    var snow:Any,

    @SerializedName("clouds")
    @Expose
    var clouds:Clouds,

    @SerializedName("weather")
    @Expose
    var weather:List<Weather>
){
    fun getData():String{
        val date = Date(dt * 1000L) // *1000 is to convert seconds to milliseconds
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss z") // the format of your date
       // sdf.setTimeZone(TimeZone.getTimeZone("GMT-4")) // give a timezone reference for formating (see comment at the bottom
        val formattedDate = sdf.format(date)
       return formattedDate
    }
}

data class Main (
    @SerializedName("temp")
    @Expose
    var temp:Double,

    @SerializedName("pressure")
    @Expose
    var pressure:Int,

    @SerializedName("humidity")
    @Expose
    var humidity:Int,

    @SerializedName("temp_min")
    @Expose
    var tempMin:Double,

    @SerializedName("temp_max")
    @Expose
    var tempMax:Double
)

data class Sys (
    @SerializedName("country")
    @Expose
    var country:String
)

data class Weather (
    @SerializedName("id")
    @Expose
    var id:Int,

    @SerializedName("main")
    @Expose
    var main:String,

    @SerializedName("description")
    @Expose
    var description:String,

    @SerializedName("icon")
    @Expose
    var icon:String
){
    fun getImageURL():String {
        return "http://openweathermap.org/img/w/$icon.png"
    }
}

data class Wind (
    @SerializedName("speed")
    @Expose
    var speed:Double,

    @SerializedName("deg")
    @Expose
    var deg:Int
)