package com.danijelsudar.danijel.weatherforecast

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import kotlinx.android.synthetic.main.activity_weather_forecast.*
import kotlinx.android.synthetic.main.app_bar_weather_forecast.*
import kotlinx.android.synthetic.main.nav_header_weather_forecast.view.*
import kotlinx.android.synthetic.main.nav_item_normal.view.*

class WeatherForecastActivity : AppCompatActivity(){

    private var mAdapter: NavDrawerAdapter? = null
    private lateinit var navItem: ArrayList<DrawerItem>
    private val ITEMS_KEY = "items_key"
    private var TAG = "WeatherForecastActivity"
    private var HEADER_ID = "header"

    companion object {
        private var SELECTED = 1
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_forecast)
        setSupportActionBar(toolbar)

        setToggle()
        navItem = (savedInstanceState?.getSerializable(ITEMS_KEY) ?: setMenuItems()) as ArrayList<DrawerItem>
        nav_view_recycler_view.layoutManager = LinearLayoutManager(this)
        updateUI()
        selectedItem(navItem[SELECTED].mItemId)//set default fragment
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putSerializable(ITEMS_KEY,navItem)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        updateUI()
    }

    private fun selectedItem(id: String) {
        when (id) {
            "nav_cities" -> {
                changerFragment(CitiesFragment.newInstance(navItem[0]))
            }

            "nav_No" -> {
                changerFragment(NoFragment.newInstance())
            }
        }
    }

    private fun setToggle() {
        val toggle = object: ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                if (slideOffset != 0f)
                    mAdapter?.notifyItemChanged(0)
                super.onDrawerSlide(drawerView, slideOffset)
            }

        }

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun setMenuItems(): ArrayList<DrawerItem> {
        navItem = ArrayList<DrawerItem>()
        navItem.add(DrawerItem(mItemId = HEADER_ID))
        navItem.add(DrawerItem(mIcon = R.drawable.ic_menu_city, mTitle = resources.getString(R.string.cities_drawer_item), mItemId = "nav_cities"))
        navItem.add(DrawerItem(mIcon = R.drawable.ic_menu_no, mTitle = "Nepoznata teritorija",mItemId = "nav_No"))
        navItem[SELECTED].mIsSelected = true

        return navItem
    }

    private fun changerFragment(fragment: Fragment) {
        val fm = supportFragmentManager
        var _fragment = fm.findFragmentById(R.id.content_drawer)
        if((_fragment == null) || (_fragment.javaClass != fragment.javaClass)) {
            _fragment = fragment
            fm.beginTransaction().replace(R.id.content_drawer, _fragment).commit()
        }
    }

    private fun updateUI() {
        if(mAdapter == null) {
            mAdapter = NavDrawerAdapter(navItem)
            nav_view_recycler_view.adapter = mAdapter
        } else {
            mAdapter?.notifyDataSetChanged()
        }
    }

    private inner class NavDrawerAdapter(items: ArrayList<DrawerItem>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private var mItems = items
        private val ITEM_TIP_HEADER: Int = 0
        private val ITEM_TIP_NORMAL: Int = 1

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
            return when (viewType) {
                ITEM_TIP_HEADER -> NavHeaderViewHolder(parent, LayoutInflater.from(parent!!.context))
                else /*ITEM_TIP_NORMAL*/ -> NavItemViewHolder(parent, LayoutInflater.from(parent!!.context))
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
            val itemType = getItemViewType(position)
            when (itemType) {
                ITEM_TIP_HEADER -> (holder as NavHeaderViewHolder).bind(mItems[position])
                ITEM_TIP_NORMAL -> {
                    (holder as NavItemViewHolder).bind(mItems[position])
                    (holder as NavItemViewHolder).itemView?.setOnClickListener {
                        if(SELECTED != position){
                            mItems[position].mIsSelected = true
                            mItems[SELECTED].mIsSelected = false
                            notifyItemChanged(position)
                            notifyItemChanged(SELECTED)
                            SELECTED = position
                        }

                        selectedItem(mItems[position].mItemId)
                        drawer_layout.closeDrawer(nav_view)
                    }
                }
            }
        }

        override fun getItemCount(): Int = mItems.size

        override fun getItemViewType(position: Int): Int
            = if(mItems[position].mItemId == HEADER_ID) ITEM_TIP_HEADER else ITEM_TIP_NORMAL
    }

    private inner class NavItemViewHolder(parent: ViewGroup?, inflater: LayoutInflater):
            RecyclerView.ViewHolder(inflater.inflate(R.layout.nav_item_normal, parent, false)){

        private lateinit var mItem: DrawerItem

        private fun setSelected(){
            itemView?.title_nav_item?.setTextColor(resources.getColor(R.color.colorPrimary))
            itemView?.img_item_nav?.setColorFilter(resources.getColor(R.color.colorPrimary))
            itemView.setBackgroundColor(resources.getColor(R.color.selected))
        }

        fun bind(item: DrawerItem){
            mItem = item
            itemView?.title_nav_item?.text = mItem?.mTitle
            itemView?.img_item_nav?.setImageResource(mItem?.mIcon as Int)
            if(mItem.mIsSelected) setSelected()
        }
    }


    private inner class NavHeaderViewHolder(parent: ViewGroup?, inflater: LayoutInflater):
            RecyclerView.ViewHolder(inflater.inflate(R.layout.nav_header_weather_forecast, parent, false)){

        fun bind(item: DrawerItem){
            itemView?.city_nav_drawer_header?.text = item?.mTitle ?: resources.getString(R.string.nav_header_drawer_no_secelect)
            itemView?.temperature_nav_drawer_header?.text = item?.mTemperature ?: ""
        }
    }
}
