package com.danijelsudar.danijel.weatherforecast.net


class ApiUtils {

    companion object {
        private val BASE_URL = "http://api.openweathermap.org/"

        @JvmStatic
        fun getAPIService(): APIService {
            return RetrofitClient.getClient(BASE_URL)!!.create(APIService::class.java!!)
        }
    }
}