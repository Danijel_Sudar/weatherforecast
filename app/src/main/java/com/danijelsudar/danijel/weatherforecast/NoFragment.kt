package com.danijelsudar.danijel.weatherforecast

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class NoFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_no, container, false)
    }



    companion object {
        fun newInstance(): NoFragment {
            val fragment = NoFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
