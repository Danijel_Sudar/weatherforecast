package com.danijelsudar.danijel.weatherforecast

import java.io.Serializable

data class DrawerItem(var mIcon: Int? = null,
                      var mTitle: String? = null,
                      var mTemperature: String? = null,
                      var mItemId: String,
                      var mIsSelected:Boolean = false): Serializable