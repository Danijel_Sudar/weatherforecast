package com.danijelsudar.danijel.weatherforecast

data class City(var mID: String?,
                var mCity:String,
                var mDate: String?,
                var mIsChecked: Boolean = false,
                var mImage: String?,
                var mWeather: String)