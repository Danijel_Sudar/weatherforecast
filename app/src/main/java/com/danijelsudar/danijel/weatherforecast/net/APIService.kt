package com.danijelsudar.danijel.weatherforecast.net

import com.danijelsudar.danijel.weatherforecast.model.CityWeatherData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {

    @GET("data/2.5/find")
    fun citiesInCycle(@Query("appid") APPID: String,
                 @Query("lat") lat: String,
                 @Query("lon") lon: String,
                 @Query("cnt") cnt: Int,
                 @Query("units") units: String = "metric"
    /*,cb: Callback<CityWeather>*/
    ): Call<CityWeatherData>//Observable<CityWeatherData>
}